<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="<?= WEBROOT . "public/img/favicon-16x16.png" ?>">
    <link rel="stylesheet" href="<?= WEBROOT . "public/bootstrap4.2/css/bootstrap.min.css" ?>">
    <link rel="stylesheet" href="<?= WEBROOT . "public/css/app.css" ?>">
    <script src="<?= WEBROOT . "public/bootstrap4.2/js/jquery3.6.0.js" ?>"></script>
    <script src="<?= WEBROOT . "public/bootstrap4.2/js/bootstrap.min.js" ?>"></script>
    <title>Virtualmin manager</title>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="#">Virtualmin manager</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?= WEBROOT . "virtualmin" ?>">Domains list</a>
                    </li>
                </ul>
                <span class="navbar-text">
                    <a class="btn btn-primary" href="<?= WEBROOT . "virtualmin/create" ?>">New domain</a>
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            My account
                        </button>
                        <div class="dropdown-menu dropdown-menu-left">
                            <div class="p-3">
                                <p><small>Hostname :</small><br/><span><?= $_SESSION['user']['hostname'] ?></span></p>
                                <p><small>Username : </small><br/><span><?= $_SESSION['user']['username'] ?></span></p>
                            </div>
                            <div class="logout_btn" class="text-center">
                                <a href="<?= WEBROOT."Auth/logout" ?>" class="dropdown-item cursor-pointer text-danger" type="button">Logout</a>
                            </div>
                        </div>
                    </div>
                </span>
            </div>
        </div>
    </nav>

    <div class="container">
        <?php
            if (!empty($flash)) :?>
                <div class="alert alert-<?php echo $flash['type'] ?> alert-dismissible fade show mt-4 <?= url_segments(2) == 'create' ? 'col-md-10' : '' ?>" role="alert">
                    <?php echo $flash["message"] ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif;
        ?>
        <?php
        if (!empty($errors)) : ?>
            <div class="alert alert-danger alert-dismissible fade show mt-4 <?= url_segments(2) == 'create' ? 'col-md-10' : '' ?>" role="alert">
                <?php
                foreach ($errors as $index => $description) {
                    echo "<li>" . $description . "</li>";
                } ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif;
        ?>
        <?php echo $content ?>
    </div>

</body>

</html>