<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="<?= WEBROOT . "public/img/favicon-16x16.png" ?>">
    <link rel="stylesheet" href="<?= WEBROOT . "public/bootstrap4.2/css/bootstrap.min.css" ?>">
    <link rel="stylesheet" href="<?= WEBROOT . "public/css/app.css" ?>">
    <script src="<?= WEBROOT . "public/bootstrap4.2/js/jquery3.6.0.js" ?>"></script>
    <script src="<?= WEBROOT . "public/bootstrap4.2/js/bootstrap.min.js" ?>"></script>
    <title>Virtualmin manager</title>
</head>

<body>

    <div class="container">
        <?php
        if (!empty($errors)) : ?>
            <div class="alert alert-danger alert-dismissible fade show mt-4" role="alert">
                <?php
                foreach ($errors as $index => $description) {
                    echo "<li>" . $description . "</li>";
                } ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif;
        ?>
        <?php echo $content ?>
    </div>

</body>

</html>