<div class="mt-5">
    <form method="POST" action="create">
        <div class="form-group row">
            <div class="col-sm-10">
                <label for="inputDomain" class="col-form-label">Domain</label>
                <input type="text" name="domain" class="form-control <?= $this->has_error('domain') ? 'is-invalid' : '' ?>" id="inputDomain" placeholder="eg. test2.lab7.evoludata.com" value="<?= $this->old('domain') ?>">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <label for="inputDesc" class="col-form-label">Desc</label>
                <input type="text" name="desc" class="form-control <?= $this->has_error('desc') ? 'is-invalid' : '' ?>" id="inputDesc" placeholder="Desc" value="<?= $this->old('desc') ?>"">
            </div>
        </div>
        <div class=" form-group row">
                <div class="col-sm-10">
                    <label for="inputPassword" class="col-form-label">Password</label>
                    <input type="password" name="password" class="form-control <?= $this->has_error('password') ? 'is-invalid' : '' ?>" id="inputPassword" placeholder="Password">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <label for="inputEmail" class="col-form-label">Email</label>
                    <input type="text" name="email" class="form-control <?= $this->has_error('email') ? 'is-invalid' : '' ?>" id="inputEmail" placeholder="eg. test@gmail.com" value="<?= $this->old('email') ?>">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-5">
                    <label for="inputDb" class="col-form-label">Database</label>
                    <input type="text" name="db" class="form-control <?= $this->has_error('db') ? 'is-invalid' : '' ?>" id="inputDb" placeholder="Database name" value="<?= $this->old('db') ?>">
                </div>
                <div class="col-sm-5">
                    <label for="inputBranch" class="col-form-label">Tiki branch</label>
                    <select name="branch" class="form-control <?= $this->has_error('branch') ? 'is-invalid' : '' ?>" id="inputBranch">
                        <option value="">Select a branch</option>
                        <option value="master">Master</option>
                        <option value="24.x">24.x</option>
                        <option value="23.x">23.x</option>
                        <option value="22.x">22.x</option>
                    </select>
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-7"></div>
                <div id="save" class="col-sm-3">
                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                </div>
                <div id="loader" class="col-sm-3 text-center d-none">
                    <img class="" width="40" src="<?= WEBROOT . 'public/img/loader.gif' ?>" alt="loader" />
                </div>
                <div id="alert-warngin" class="col-sm-10 d-none">
                    <div class="alert alert-warning alert-dismissible fade show mt-4" role="alert">
                        <span>This process may take several minutes, please wait.</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
    </form>
</div>

<script>
    $("#save").click(function() {
        $("#save").addClass("d-none");
        $("#loader").removeClass("d-none");
        $("#alert-warngin").removeClass("d-none");
    });
</script>