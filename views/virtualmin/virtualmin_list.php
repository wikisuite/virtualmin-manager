<div class="mt-5 table-responsive">
    <table id="table-domains" class="table table-bordered">
        <thead class="">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Server</th>
                <th scope="col">Damain name</th>
                <th scope="col">Type</th>
                <th scope="col">Plan</th>
                <th scope="col">Username</th>
                <th scope="col">Espace utilisé/disponible</th>
                <th scope="col">Status</th>
                <th scope="col">Certify</th>
            </tr>
        </thead>
        <tbody id="domains">
            <?php
            $i = 1;
            foreach ($domains as $row) :
            ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $row['server'] ?></td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['type'] ?></td>
                    <td><?= $row['plan'] ?></td>
                    <td><?= $row['username'] ?></td>
                    <td><?= $row['disk_space_used'] . "/" . $row['disk_space_limit'] ?></td>
                    <td><label class="badge badge-<?= $row['status'] == 'active' ? 'primary' : 'danger' ?>"><?= !empty($row['status']) ? $row['status'] : "---"  ?></label></td>
                    <td><a class="btn btn-success" href="<?=WEBROOT.'virtualmin/certify/'.str_replace('.', '--', $row['name']).'/'. $row['username'] ?>">Certify</a></td>
                </tr>
            <?php
                $i++;
            endforeach;
            ?>
        </tbody>
    </table>
    <div class="text-center" id="loader">
        <img width="70" src="<?= WEBROOT . 'public/img/loader.gif' ?>" alt="loader" />
        <p>Loading domains...</p>
    </div>
</div>

<script>
    $.get('<?= WEBROOT."virtualmin/ajaxLoadData/domains" ?>', function(data) {
        $("#domains").html(data);
        $("#loader").addClass("d-none");
    });
</script>