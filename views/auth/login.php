<div>
    <form action="<?= WEBROOT . "auth/login" ?>" method="post">
        <div class="container">
            <div class="row justify-content-center align-items-center" style="height:100vh">
                <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                    <div class="card shadow-2-strong" style="border-radius: 1rem;">
                        <div class="card-body p-5 text-center">
                            <img class="img-fluid" src="<?= WEBROOT . 'public/img/EvoluData-logo-web.png'?>" alt="Logo">
                            <h3 class="">Sign in</h3>
                            <p class="mb-3">You must enter a username and password to login</p>
                            <div class="form-outline mb-4">
                                <input disabled type="text" id="hostname" name="hostname" class="form-control" placeholder="Hostname" value="<?=$default_credentials['hostname']?>"/>
                            </div>
                            <div class="form-outline mb-4">
                                <input type="test" id="username" name="username" class="form-control" placeholder="Username" value="<?=$this->old('username')?>"/>
                            </div>
                            <div class="form-outline mb-4">
                                <input type="password" id="Password" name="password" class="form-control" placeholder="Password" autocomplete="off" />
                            </div>
                            <button class="btn btn-primary btn-block" id="login" name="login" type="submit">
                                <span id="spinner" class="spinner-border-sm" role="status" aria-hidden="true"></span>
                                <span>Login</span>
                            </button>
                            <?php if(!empty($message)) : ?>
                                <div class="alert alert-<?=$message['type']?> alert-dismissible fade show mt-3" role="alert">
                                    <strong>Message: </strong><?=$message['message']?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $("#login").click(function (){
        $("#spinner").addClass("spinner-border");
        $("#loader").removeClass("d-none");
    });
</script>