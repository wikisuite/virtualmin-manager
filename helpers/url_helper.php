<?php

if (!function_exists('url_segments'))
{
	/**
	 * URL SEGMENT
	 * Returns the segment define in url if $level is set, else return array of all url segment
	 *
	 * @param	int	$level => base_url/level1/level2/.../levelX
	 * @return	string|array 
	 */
	function url_segments($level = null)
	{
		$url = explode('/',$_GET['p']);
        if (isset($level) and is_numeric($level)) {
            if (count($url) >= $level) return $url[$level-1];
            else return null;
        }
        return $url;
	}
}


if (!function_exists('current_url'))
{
	/**
	 * Current URL
	 *
	 * Returns the full URL (including segments) of the page where this
	 * function is placed
	 *
	 * @return	string
	 */
	function current_url()
	{
		return $_SERVER['REQUEST_URI'];
	}
}
