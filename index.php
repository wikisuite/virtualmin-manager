<?php

use Symfony\Component\Dotenv\Dotenv;

define('WEBROOT', str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));

require("vendor/autoload.php");

require(ROOT."core/Controller.php");

/** Load .env file  */
$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');

/** Load helpers functions */
require(ROOT."helpers/url_helper.php");


$params = explode('/',$_GET['p']);
$controller = $params[0] ? $params[0] : 'auth';
$action = !empty($params[1]) ? $params[1] : ($controller == 'auth' ? "login": "index");

require("controllers/".ucwords($controller).".php");

$controller = new $controller;

if (method_exists($controller, $action)) {
    unset($params[0]); 
    unset($params[1]);
    call_user_func_array(array($controller, $action), $params);
} 