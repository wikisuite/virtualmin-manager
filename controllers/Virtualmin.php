<?php

use \FintechSystems\VirtualminApi\VirtualminApi;

class Virtualmin extends Controller
{

    function __construct()
    {
        $this->is_not_logged();
        $this->layout = "admin_layout";
        $this->set_default_credentials();
        $this->api = $_SESSION['vm_api'];
        $this->server = $_SESSION['user'];
    }

    function index()
    {
        $this->list();
    }

    function list()
    {
        if (isset($_SESSION['flash'])) {
            $data['flash'] = $_SESSION['flash'];
            unset($_SESSION['flash']);
            $this->assign($data);
        }
        $domains = $_SESSION['domains'];
        $data['domains'] = $domains;
        $this->assign($data);
        $this->loadView("virtualmin_list");
    }


    function create($id = null)
    {
        if (isset($_SESSION['flash'])) {
            $data['flash'] = $_SESSION['flash'];
            unset($_SESSION['flash']);
            $this->assign($data);
        }
        if ($_POST) {
            $this->store();
            exit;
        }
        $this->loadView("virtualmin_create");
    }

    function store()
    {
        if ($_POST) {
            $fields = array(
                'domain' => "please enter the domain",
                'desc' => "please enter the description",
                'password' => "please enter the password",
                'email' => "please enter the email",
                'db' => "please enter db",
                'branch' => "Please select a valid branch"
            );

            $errors = $this->validate_fields($fields, $_POST);
            if (!empty($errors)) {
                $data['old'] = $_POST;
                $data['errors'] = $errors;
                $this->assign($data, 'error');
                $this->loadView("virtualmin_create");
            }

            extract($_POST);

            $info = [
                // New virtual server details
                'domain'                 => $domain,  // user input
                'desc'                   => $desc,  // user input
                'pass'                   => $password,  // computed
                'template'               => 'WikiSuite 1',  // hardcoded
                'plan'                   => 'WikiSuite 1',  // hardcoded

                // Advanced options
                'email'                  => $email,  // user input
                'db'                     => $db,      // computed

                // Enabled features
                'features-from-plan'     => '',
                'virtualmin-tikimanager' => '',
            ];
            $result = $this->api->createDomain($info);
            $result = json_decode($result, true);
            if ($result['status'] == 'failure') {
                $str = $result['error'];
                $data['errors'] = ['Error, '.$str];
                $this->assign($data);
                $this->loadView("virtualmin_create");
                
            }
            $this->create_flash("Domain created successfully!", 'success');
            // Create Tiki Instance
            $result =  $this->createTikiInstance($domain, $password, $branch);
            if (!empty($result['error'])) {
                $data['errors'] = [ 'Error, Tiki instance installation failed after domain created: '.$result['error'] ];
                $this->assign($data);
                $this->redirect('virtualmin/create');
            }
            $this->create_flash("Domain created and Tiki installed successfully!", 'success');
            $this->redirect('virtualmin');
        } else {
            $this->redirect('virtualmin/create');
        }
    }

    function ajaxLoadData($data = 'domains')
    {
        if ($data == 'domains') {
            $data = "";
            $domains = $this->api->getDomains();
            $_SESSION['domains'] = $domains;
            $i = 1;
            foreach ($domains as $row) :
                $data .= '<tr>' .
                    "<td>$i</td>" .
                    "<td>" . $row['server'] . "</td>" .
                    "<td>" . $row['name'] . "</td>" .
                    "<td>" . $row['type'] . "</td>" .
                    "<td>" . $row['plan'] . "</td>" .
                    "<td>" . $row['username'] . "</td>" .
                    "<td>" . $row['disk_space_used'] . "/" . $row['disk_space_limit'] . "</td>" .
                    "<td><span class='badge badge-primary'>" . $row['status'] . "</span></td>" .
                    "<td><a class='btn btn-success' href='".WEBROOT.'virtualmin/certify/'.str_replace('.', '--', $row['name']).'/'. $row['username']."'>Certify</a></td>".
                    "</tr>";
                $i++;
            endforeach;

            echo $data;
        }
    }

    function extract_error_msg($str) {
        
        $delimiter = '#';
        $startTag = 'line 60.';
        $endTag = 'Adds a new Virtualmin';
        $regex = $delimiter . preg_quote($startTag, $delimiter) 
                            . '(.*?)' 
                            . preg_quote($endTag, $delimiter) 
                            . $delimiter 
                            . 's';
        if (preg_match($regex,$str,$matches)) {
            return $matches[1];
        }
        return false;
    }

    public function createTikiInstance($domain = null, $password = null, $branch = '24.x') {
        $data = [
            // New virtual server details
            'domain'                 => $domain,
            'branch'                   => $branch,
            'password'                   => $password
        ];
        $result = $this->installProgram('tiki-install', $data);
        return json_decode($result, true);
    }

    private function installProgram($program, $data = null)
    {
        $hostname = $this->server['hostname'];
        $username = $this->server['username'] ?? 'root';
        $password = $this->server['password'];
        $port = $this->server['port'] ?? '10000';

        ray('runProgram server', $this->server);
        
        $url = "https://$hostname/virtual-server/remote.cgi?json=1&multiline&program=$program";

        if (is_array($data) && ! empty($data)) {
            $url .= '&'.http_build_query($data);
        }
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PORT, $port);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Basic '.base64_encode($username.':'.$password),
        ]);

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3000);

        ray()->measure();
        $result = curl_exec($ch);
        ray()->measure();
        
        return $result;
    }

    public function certify ($domain, $username) 
    {
        $data = [
            // New virtualmin install-cert command details
            'domain'  => str_replace('--', '.', $domain),
            'cert'    => "/home/$username/ssl.cert",
            'key'     => "/home/$username/ssl.key",
            'ca'      => "/home/$username/ssl.ca" 
        ];
        $result = $this->installProgram('install-cert', $data);
        $response = json_decode($result);
        $type = $response->status == 'success' ? 'success' : 'danger';
        $message = $response->error;
        $this->create_flash($result, $type);
        $this->redirect('virtualmin');
    }
}
