<?php

use \FintechSystems\VirtualminApi\VirtualminApi;

class Auth extends Controller
{

    function __construct()
    {
        $this->set_default_credentials();
    }


    function login()
    {
        $this->is_logged();
        $message = '';

        if (isset($_POST['login']) && isset($_POST['username']) && isset($_POST['password'])) {
            function validate($data)
            {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
            extract($_POST);
            $username = validate($username);
            $password = validate($password);

            if (empty($username)) {
                $message = ['message' => 'Username is required', 'type' => 'warning'];
            } elseif (empty($password)) {
                $message = ['message' => 'Password is required', 'type' => 'warning'];
            } else {

                if ($username != $this->get_default_credentials()['username_demo'] || $password != $this->get_default_credentials()['password_demo']) {
                    $message = ['message' => 'Your credentials are wrong', 'type' => 'danger'];
                } else {
                    $api = new VirtualminApi(
                        [
                            'hostname' => $this->get_default_credentials()['hostname'],
                            'username' => $this->get_default_credentials()['username'],
                            'password' => $this->get_default_credentials()['password']
                        ],
                        'debug'
                    );
                    $domains = $api->getDomains();

                    if (count($domains) != 0) {
                        $_SESSION['user'] = [
                            'hostname' => $this->get_default_credentials()['hostname'],
                            'username' => $this->get_default_credentials()['username'],
                            'password' => $this->get_default_credentials()['password']
                        ];
                        $_SESSION['domains'] = $domains;
                        $_SESSION['vm_api'] = $api;
                        $this->create_flash("Logged In successfully", 'success');
                        $this->redirect("virtualmin");
                    } else {
                        $message = ['message' => 'Unable to connect to the server, please contact the admin if the problem persists', 'type' => 'danger'];
                    }
                }
            }
        }
        $data['old'] = $_POST;
        $data['message'] = $message;
        $data['default_credentials'] = $this->get_default_credentials();
        $this->assign($data);
        $this->loadView("login");
    }

    function logout()
    {
        session_destroy();
        $this->redirect("");
    }
}
