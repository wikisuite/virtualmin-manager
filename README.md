## Project status
As of 2022-04, this project is on hold while we focus on https://doc.tiki.org/Tiki-Manager-Package

# Virtualmin Manager

A PHP script to leverage the Virtualmin API

## Before installation
* Make sure Composer is installed

## Installation
1. Download the repo or clone it.
2. In repo root run "composer install"
3. Create an .env file with all the variables. The .env.template is provided as a sample file.

