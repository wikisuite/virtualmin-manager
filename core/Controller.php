<?php

session_start();

class Controller {

    public $data = [];
    public $layout = 'default';
    private $redirect = 'virtualmin';

    public function assign($data) {
        $this->data = array_merge($this->data, $data);
    }

    public function loadView($view) {
        extract($this->data);
        ob_start();
        require(ROOT."views/".strtolower(get_class($this))."/".$view.".php");
        $content = ob_get_clean();
        require_once(ROOT."views/layout/".$this->layout.".php");
        exit;
    }

    public function has_error($field) {
        $errors = $this->data['errors'];
        if (!empty($errors)) {
            foreach ($errors as $index=> $error) {
                if ($index == $field) {
                    return true;
                }
            }
        }
        return false;
    }

    public function old($field) {
        $data = !empty($this->data['old']) ?$this->data['old']:null;
        if ($data) {
            if (array_key_exists($field, $data)) {
                return $data[$field];
            }
        }
        return '';
    }

    public function validate_fields($fields, $data) {
        $errors = [];
        foreach ($fields as $field => $error) {
            if(empty($data[$field])) {
                $errors[$field] = $error; 
            }
        }
        return $errors;
    }
    
    public function redirect($page) {
        if (!empty($this->data['flash'])) {
            $_SESSION['flash'] = $this->data['flash'];
        }
        header('Location: ' .WEBROOT .$page);
        exit();
    }

    public function is_not_logged() {
        if (!isset($_SESSION['user']) && empty($_SESSION['user'])) {
            $this->redirect('');
        }
    }

    public function is_logged() {
        if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {
            $this->redirect($this->redirect);
        }
    }

    public function create_flash($message, $type = 'danger') {
        $this->data['flash'] = [
            'message' => $message,
            'type' => $type
        ];
    }

    function set_default_credentials()
    {
        $this->hostname = $_ENV['ROOT_HOSTNAME'];
        $this->username = $_ENV['ROOT_USER'];
        $this->password = $_ENV['ROOT_PASSWD'];
        $this->username_demo = $_ENV['DEMO_USER'];
        $this->password_demo = $_ENV['DEMO_PASSWD'];
    }

    function get_default_credentials()
    {
        return [
            'hostname' => $this->hostname,
            'username' => $this->username,
            'password' => $this->password,
            'username_demo' => $this->username_demo,
            'password_demo' => $this->password_demo
        ];
    }

}